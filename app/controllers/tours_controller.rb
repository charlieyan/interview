class ToursController < ApplicationController
  before_action :set_tour, only: [:show, :update, :destroy]

  # GET /tours
  # GET /tours.json
  def index
    @tours = Tour.all

    render json: @tours
  end

  # GET /tours/1
  # GET /tours/1.json
  def show
    render json: @tour
  end

  # POST /tours
  # POST /tours.json
  def create
    data      = tour_params
    property  = Property.find(data[:property])
    if (property == nil)
      render json: @tour.errors, status: :unprocessable_entity
    end
    data[:property] = property
    data[:company]  = property.company
    @tour = Tour.new(data)
    if @tour.save
      render json: @tour, status: :created, location: @tour
    else
      render json: @tour.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tours/1
  # PATCH/PUT /tours/1.json
  def update
    @tour = Tour.find(params[:id])

    if @tour.update(tour_params)
      head :no_content
    else
      render json: @tour.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tours/1
  # DELETE /tours/1.json
  def destroy
    @tour.destroy

    head :no_content
  end

  private

    def set_tour
      @tour = Tour.find(params[:id])
    end

    def tour_params
      params.require(:tour).permit(:property, :company, :attending, :tourTime, :tourDuration)
    end
end
