# @class ApplicationController
# @author Evan
class ApplicationController < ActionController::API
  def current_company
    Company.first
  end
end
