# @class Stay
# @author Evan
class Stay < ActiveRecord::Base
  belongs_to :property

  has_many :tenants

  scope :starts_before, -> (date) { where('start_date <= ?', date) }
  scope :ends_after, -> (date) { where('end_date >= ?', date) }

  def self.only_current(d)
    starts_before(d).ends_after(d)
  end
end
