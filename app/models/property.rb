# @class Company
# @author Evan
class Property < ActiveRecord::Base
  belongs_to :company

  has_many :stays

  def self.current_stays()
    stays.only_current(Date.today)
  end

  def self.current_stays(d)
    stays.only_current(d)
  end
end
