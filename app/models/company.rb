# @class Company
# @author Evan
class Company < ActiveRecord::Base
  has_many :properties
  has_many :tours
end
