object @property
attributes :id,
           :street_address,
           :beds,
           :baths,
           :rent,
           :company_id,
           :created_at,
           :updated_at
