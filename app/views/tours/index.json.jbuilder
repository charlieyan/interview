json.array!(@tours) do |tour|
  json.extract! tour, :id, :property, :company, :attending, :tourTime, :tourDuration
  json.url tour_url(tour, format: :json)
end
