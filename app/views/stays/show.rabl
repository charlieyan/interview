object @stay
attributes :id,
           :start_date,
           :end_date,
           :property_id,
           :created_at,
           :updated_at
