c = Company.create(name: 'Test Company',
                   phone: '1234567890')

p = Property.create(street_address: '1052 Baldwin',
                    beds: 10,
                    baths: 4.5,
                    rent: 990_000,
                    company: c)

s = Stay.create(start_date: 1.month.ago,
                end_date: 10.months.from_now,
                property: p)

Tenant.create(name: 'Tim Jones',
              email: 'tim@cribspot.com',
              stay: s)
