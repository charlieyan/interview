class CreateTours < ActiveRecord::Migration
  def change
    create_table :tours do |t|
      t.references     :property
      t.references     :company
      t.string         :attending
      t.datetime       :tourTime
      t.integer        :tourDuration
      t.timestamps null:false
    end
  end
end
