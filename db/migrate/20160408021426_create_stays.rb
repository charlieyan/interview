class CreateStays < ActiveRecord::Migration
  def change
    create_table :stays do |t|
      t.date :start_date
      t.date :end_date
      t.references :property, index: true

      t.timestamps null: false
    end
    add_foreign_key :stays, :properties
  end
end
