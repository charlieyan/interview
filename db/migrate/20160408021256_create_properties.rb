class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :street_address
      t.integer :beds
      t.float :baths
      t.integer :rent
      t.references :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :properties, :companies
  end
end
